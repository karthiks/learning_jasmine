// Source: https://github.com/kjbekkelund/karma-requirejs/blob/master/test/test-main.js
// Forked URL: https://github.com/karthiks/karma-requirejs/blob/master/test/test-main.js

//Just like any Require.js project, you need a main module to bootstrap your tests. We do this here..

var tests = [];
for (var file in window.__karma__.files) {
    if (/Spec\.js$/.test(file)) {
        tests.push(file);
    }
}

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/src',

    //paths: {},
    //shim: {},

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});
