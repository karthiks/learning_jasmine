define(['hello'], function(Hello) {
//
  describe("helloWorld", function() {
    it("should return hello world", function() {
      expect(Hello.helloWorld()).toEqual('Hello World');
    });
  });

  describe("helloAnyone", function() {
    it("should return hello and any value", function() {
      expect(Hello.helloAnyone('Jeffrey')).toEqual('Hello Jeffrey');
      expect(Hello.helloAnyone('John')).toEqual('Hello John');
    });
  });

});