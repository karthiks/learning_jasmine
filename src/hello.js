define(function() {

  var Hello = {};

  Hello.helloWorld = function() {
    return 'Hello World';
  };

  Hello.helloAnyone = function(person) {
    return 'Hello ' + person;
  };

  return Hello;
});